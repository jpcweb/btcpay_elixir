defmodule BTCPay.Invoice do
  @moduledoc """
  https://docs.btcpayserver.org/API/Greenfield/v1/#tag/Invoices
  """
  alias BTCPay.Encoder
  alias BTCPay.Decoder
  alias BTCPay.Invoice

  @derive Jason.Encoder
  defstruct id: nil,
            status: nil,
            additionalStatus: nil,
            monitoringExpiration: nil,
            expirationTime: nil,
            createdTime: nil,
            amount: nil,
            currency: nil,
            metadata: nil,
            checkout: nil

  def all(store_id) do
    endpoint = "/api/v1/stores/#{store_id}/invoices"

    with {:ok, %{status_code: 200, body: body}} <- http_client().get(endpoint) do
      {:ok, Decoder.decode(Invoice, body)}
    else
      error -> {:error, error}
    end
  end

  def get(%Invoice{id: id}, store_id), do: get(id, store_id)

  def get(id, store_id) do
    endpoint = "/api/v1/stores/#{store_id}/invoices/#{id}"

    with {:ok, %{status_code: 200, body: body}} <- http_client().get(endpoint) do
      {:ok, Decoder.decode(Invoice, body)}
    else
      error -> {:error, error}
    end
  end

  def create(%Invoice{} = invoice, store_id) do
    endpoint = "/api/v1/stores/#{store_id}/invoices"

    with {:ok, data} <- Encoder.encode(invoice),
         {:ok, %{status_code: 200, body: body}} <- http_client().post(endpoint, data) do
      {:ok, Decoder.decode(Invoice, body)}
    else
      error -> {:error, error}
    end
  end

  def pay_url(%Invoice{id: id}) do
    "https://#{Application.get_env(:btcpay, :host)}/i/#{id}"
  end

  defp http_client(), do: Application.get_env(:btcpay, :http_client, BTCPay.Client)
end
