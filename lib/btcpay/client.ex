defmodule BTCPay.Client do
  @moduledoc """
  Client for sending HTTP requests to the BTCPay server.
  It's basically a wrapper around HTTPoison with auth capabilities.
  """
  use HTTPoison.Base

  def process_request_url(endpoint) do
    "https://#{Application.get_env(:btcpay, :host)}#{endpoint}"
  end

  def process_request_headers(headers) when is_map(headers) do
    headers
    |> Enum.into([])
    |> process_request_headers()
  end

  def process_request_headers(headers) do
    api_key = Application.get_env(:btcpay, :api_key)

    headers
    |> Keyword.put_new(:Accept, "application/json")
    |> Keyword.put_new(:"Content-Type", "application/json")
    |> Keyword.put_new(:Authorization, "token #{api_key}")
  end
end
